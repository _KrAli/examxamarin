﻿using Exam.Enum;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Exam.ViewModels
{
    class NavigationViewModel : ViewModelBase
    {
        public NavigationViewModel()
        {

            MessagingCenter.Send(this as ViewModelBase, "Navigation", NavigationName.LogIn);
        }

        

    }
}
