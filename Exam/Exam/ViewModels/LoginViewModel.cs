﻿using Exam.Enum;
using Exam.Interfaces;
using Exam.Services;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace Exam.ViewModels
{
    class LoginViewModel : ViewModelBase
    {
        IDataBaseRequests dataBaseRequests = new DataBaseRequests();

        IEncrypting encrypting = new Encrypting();


        public LoginViewModel()
        {
            EmailValidationError = "";
            EmailValidationError = "";
            LogInValidationError = "";
        }

        private string emailEntry;
        public string EmailEntry
        {
            get
            {
                return emailEntry;
            }
            set
            {
                emailEntry = value;
                RaisePropertyChanged(() => EmailEntry);
            }
        }

        private string passwordEntry;
        public string PasswordEntry
        {
            get
            {
                return passwordEntry;
            }
            set
            {
                passwordEntry = value;
                RaisePropertyChanged(() => PasswordEntry);
            }
        }

        private string emailValidationError;
        public string EmailValidationError
        {
            get { return emailValidationError; }
            set => Set(ref emailValidationError, value);
        }

        private string passwordValidationError;
        public string PasswordValidationError
        {
            get { return passwordValidationError; }
            set => Set(ref passwordValidationError, value);
        }

        private string logInValidationError;
        public string LogInValidationError
        {
            get { return logInValidationError; }
            set => Set(ref logInValidationError, value);
        }

        private bool isEnable;
        public bool IsEnable
        {
            get { return isEnable; }
            set => Set(ref isEnable, value);
        }

        private Command logIn;
        public Command LogIn
        {
            get
            {
                return logIn ?? (logIn = new Command(
                           () =>
                           {
                               
                                   var account = dataBaseRequests.GetAccount(EmailEntry);

                                   if (account != null)
                                   {

                                       var EnterPassword = encrypting.Encrypt(PasswordEntry, account.Salt);

                                       //var Password= encrypting.Encrypt(account.Hash, account.Salt);

                                       if (EnterPassword == account.Hash)
                                       {

                                           MessagingCenter.Send(this as ViewModelBase, "Navigation", NavigationName.EmployeeMain);
                                       }
                                       else
                                       {
                                           LogInValidationError = "Email or password is invalid";
                                       }


                                   }
                                   else
                                   {
                                       LogInValidationError = "Email or password is invalid";
                                   }
                               

                           }
                       ));
            }
        }

        private Command checkEmailAndPassword;
        public Command CheckEmailAndPassword
        {
            get
            {
                return checkEmailAndPassword ?? (checkEmailAndPassword = new Command(
                           () =>
                           {
                               if (String.IsNullOrWhiteSpace(EmailEntry)) { EmailValidationError = "A email is require!";   }
                               else if (!IsValidEmail(EmailEntry)) { EmailValidationError = "Email is invalid";  }
                               else { EmailValidationError = ""; }

                               if (String.IsNullOrWhiteSpace(PasswordEntry)) { PasswordValidationError = "A password is require!";  }
                               else { PasswordValidationError = "";  }

                               if(EmailValidationError == "" && PasswordValidationError == "") { IsEnable = true; }
                               else { isEnable = false;  }

                           }
                       ));
            }
        }

        private Command registrationCommand;
        public Command RegistrationCommand
        {
            get
            {
                return registrationCommand ?? (registrationCommand = new Command(
                           () =>
                           {
                               MessagingCenter.Send(this as ViewModelBase, "Navigation", NavigationName.Registration);
                           }
                       ));
            }
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

    }
}