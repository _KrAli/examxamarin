﻿using Exam.Enum;
using Exam.Interfaces;
using Exam.Models;
using Exam.Services;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Exam.ViewModels
{
    class EmployeeCreateViewModel : ViewModelBase
    {
        IDataBaseRequests dataBaseRequests = new DataBaseRequests();

        Accounts accounts;

        public EmployeeCreateViewModel()
        {

            GetAccount();

        }

        private void GetAccount()
        {
            MessagingCenter.Subscribe<ViewModelBase, Accounts>(this, "Account", (sender, param) => {

                accounts = param;

            });
        }

        private string phoneEntry;
        public string PhoneEntry
        {
            get { return phoneEntry; }
            set => Set(ref phoneEntry, value);
        }

        private string educationEntry;
        public string EducationEntry
        {
            get { return educationEntry; }
            set => Set(ref educationEntry, value);
        }

        private string experienceEntry;
        public string ExperienceEntry
        {
            get { return experienceEntry; }
            set => Set(ref experienceEntry, value);
        }

        private DateTime dateOfBirthDataPicker;
        public DateTime DateOfBirthDataPicker
        {
            get { return  dateOfBirthDataPicker; }
            set => Set(ref dateOfBirthDataPicker, value);
        }

        private string previousJobEntry;
        public string PreviousJobEntry
        {
            get { return previousJobEntry; }
            set => Set(ref previousJobEntry, value);
        }

        private string noteEntry;
        public string NoteEntry
        {
            get { return noteEntry; }
            set => Set(ref noteEntry, value);
        }

        private Employee CreateEmployee(int id)
        {
            Employee employee = new Employee
            {
                DateOfBirth = Convert.ToDateTime(DateOfBirthDataPicker),
                Education=EducationEntry,
                Experience= decimal.Parse(ExperienceEntry),
                AccountId =id,
                Icon=null,
                Note=NoteEntry,
                Phone=PhoneEntry,
                PreviousJob=PreviousJobEntry

            };

            return employee;
        }

        private Command signUpCommand;
        public Command SignUpCommand
        {
            get
            {
                return signUpCommand ?? (signUpCommand = new Command(
                           () =>
                           {
                               int id = dataBaseRequests.AddAccount(accounts);

                               dataBaseRequests.AddEmployee(CreateEmployee(id));

                               MessagingCenter.Send(this as ViewModelBase, "Navigation", NavigationName.EmployeeMain);
                           }
                       ));
            }
        }
    }
}
