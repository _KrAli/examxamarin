﻿using Exam.Enum;
using Exam.Interfaces;
using Exam.Models;
using Exam.Services;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace Exam.ViewModels
{
    class RegistrationViewModel : ViewModelBase
    {

        IEncrypting encrypting = new Encrypting();

        IDataBaseRequests dataBaseRequests = new DataBaseRequests();

        public RegistrationViewModel()
        {
            AccountTypes = new ObservableCollection<AccountType>(dataBaseRequests.GetAccountType());
        }

        private string fullNameEntry;
        public string FullNameEntry
        {
            get { return fullNameEntry; }
            set => Set(ref fullNameEntry, value);
        }

        private string emailEntry;
        public string EmailEntry
        {
            get { return emailEntry; }
            set => Set(ref emailEntry, value);
        }

        private string passwordEntry;
        public string PasswordEntry
        {
            get { return passwordEntry; }
            set => Set(ref passwordEntry, value);
        }

        private string rePasswordEntry;
        public string RePasswordEntry
        {
            get { return rePasswordEntry; }
            set => Set(ref rePasswordEntry, value);
        }

        #region Validation
        private string emailValidationError;
        public string EmailValidationError
        {
            get { return emailValidationError; }
            set => Set(ref emailValidationError, value);
        }

        private string passwordValidationError;
        public string PasswordValidationError
        {
            get { return passwordValidationError; }
            set => Set(ref passwordValidationError, value);
        }

        private string rePasswordValidationError;
        public string RePasswordValidationError
        {
            get { return rePasswordValidationError; }
            set => Set(ref rePasswordValidationError, value);
        }

        private bool isEnable;
        public bool IsEnable
        {
            get { return isEnable; }
            set => Set(ref isEnable, value);
        } 
        #endregion

        private AccountType selectAccountType;
        public AccountType SelectAccountType
        {
            get { return selectAccountType; }
            set
            {
                Set(ref selectAccountType, value);

                if (EmailValidationError == "" && PasswordValidationError == "" && RePasswordValidationError == "" && !ExistEmail && SelectAccountType != null) { IsEnable = true; }
                else { isEnable = false; }
            }
        }

        private ObservableCollection<AccountType> accountTypes;
        public ObservableCollection<AccountType> AccountTypes
        {
            get { return accountTypes; }
            set => Set(ref accountTypes, value);
        }


        private Accounts AccountCreate()
        {

            string salt = encrypting.CreateSalt();

            string hash = encrypting.Encrypt(PasswordEntry, salt);

            Accounts accounts = new Accounts
            {
                Email = EmailEntry,
                Fullname = FullNameEntry,
                Hash = hash,
                Salt = salt,
                TypeId = selectAccountType.Id
            };

            return accounts;
        }

        private Command nextCommand;
        public Command NextCommand
        {
            get
            {
                return nextCommand ?? (nextCommand = new Command(
                           () =>
                           {
                               if (NavigationName.EmployeeCreate.ToString().Contains(SelectAccountType.Name))
                               {
                                   MessagingCenter.Send(this as ViewModelBase, "Navigation", NavigationName.EmployeeCreate);
                               }
                               else if (NavigationName.EmployerCreate.ToString().Contains(SelectAccountType.Name))
                               {
                                   MessagingCenter.Send(this as ViewModelBase, "Navigation", NavigationName.EmployerCreate);
                               }

                               MessagingCenter.Send(this as ViewModelBase, "Account", AccountCreate());

                           }
                       ));
            }
        }

        #region Validation
        private Command checkEmailAndPassword;
        public Command CheckEmailAndPassword
        {
            get
            {
                return checkEmailAndPassword ?? (checkEmailAndPassword = new Command(
                           () =>
                           {
                               if (String.IsNullOrWhiteSpace(EmailEntry)) { EmailValidationError = "A email is require!"; }
                               else if (!IsValidEmail(EmailEntry)) { EmailValidationError = "Email is invalid";  }
                               else if (!ExistEmail) { EmailValidationError = ""; }

                               if (String.IsNullOrWhiteSpace(PasswordEntry)) { PasswordValidationError = "A password is require!"; }
                               else { PasswordValidationError = ""; }

                               if (String.IsNullOrWhiteSpace(RePasswordEntry)) { RePasswordValidationError = "Repeat the password!";  }
                               else if (RePasswordEntry != PasswordEntry) { RePasswordValidationError = "Password is not correct!";}
                               else { RePasswordValidationError = "";}


                               if (EmailValidationError == "" && PasswordValidationError == "" && RePasswordValidationError == "" && !ExistEmail && SelectAccountType != null) { IsEnable = true; }
                               else { isEnable = false; }
                           }
                       ));
            }
        }

        private Command isSameEmail;
        public Command IsSameEmail
        {
            get
            {
                return isSameEmail ?? (isSameEmail = new Command(
                           () =>
                           {
                               if (dataBaseRequests.isSameEmail(EmailEntry)) { EmailValidationError = "This email already registered!"; ExistEmail = true; }
                               else { EmailValidationError = ""; ExistEmail = false; }

                               if (EmailValidationError == "" && PasswordValidationError == "" && RePasswordValidationError == "" && !ExistEmail && SelectAccountType != null) { IsEnable = true; }
                               else { isEnable = false; }
                           }
                       ));
            }
        }

        private bool existEmail;
        public bool ExistEmail
        {
            get { return existEmail; }
            set => Set(ref existEmail, value);
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        #endregion
    }
}
