﻿using Exam.Interfaces;
using Exam.Models;
using Exam.Services;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Linq;
using Xamarin.Forms;
using Exam.Enum;

namespace Exam.ViewModels
{
    class EmployerCreateViewModel : ViewModelBase
    {

        IDataBaseRequests dataBaseRequests = new DataBaseRequests();
        Accounts accounts;

        public EmployerCreateViewModel()
        {
            GetAccount();

            Cities = new ObservableCollection<Cities>(dataBaseRequests.GetCities());
        }

        private void GetAccount()
        {
            MessagingCenter.Subscribe<ViewModelBase, Accounts>(this, "Account", (sender, param) => {

                accounts = param;

            });
        }

       
        private string searchCitiEntry;
        public string SearchCitiEntry
        {
            get { return searchCitiEntry; }
            set => Set(ref searchCitiEntry, value);
        }

        private string contactsEntry;
        public string ContactsEntry
        {
            get { return contactsEntry; }
            set => Set(ref contactsEntry, value);
        }

        private string companyNameEntry;
        public string CompanyNameEntry
        {
            get { return companyNameEntry; }
            set => Set(ref companyNameEntry, value);
        }

        private string websiteEntry;
        public string WebsiteEntry
        {
            get { return websiteEntry; }
            set => Set(ref websiteEntry, value);
        }

        private string noteEntry;
        public string NoteEntry
        {
            get { return noteEntry; }
            set => Set(ref noteEntry, value);
        }


        private Cities selectCities;
        public Cities SelectCities
        {
            get { return selectCities; }
            set => Set(ref selectCities, value);
        }

        private ObservableCollection<Cities> cities;
        public ObservableCollection<Cities> Cities
        {
            get { return cities; }
            set => Set(ref cities, value);
        }

        private Employer CreateEmployer(int id)
        {
            Employer employer = new Employer
            {
                AccountId=id,
                CityId=SelectCities.Id,
                CompanyName=CompanyNameEntry,
                Contacts=ContactsEntry,
                Note=NoteEntry,
                WebSite=WebsiteEntry                

            };

            return employer;
        }


        private Command signUpCommand;
        public Command SignUpCommand
        {
            get
            {
                return signUpCommand ?? (signUpCommand = new Command(
                           () =>
                           {
                               int id = dataBaseRequests.AddAccount(accounts);

                               dataBaseRequests.AddEmployer(CreateEmployer(id));

                               MessagingCenter.Send(this as ViewModelBase, "Navigation", NavigationName.LogIn);
                           }
                       ));
            }
        }

    }
}
