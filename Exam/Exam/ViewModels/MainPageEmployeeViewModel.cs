﻿using Exam.Interfaces;
using Exam.Models;
using Exam.Services;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace Exam.ViewModels
{
    class MainPageEmployeeViewModel : ViewModelBase
    {

        IDataBaseRequests dataBaseRequests = new DataBaseRequests();
        List<Vacancy> Vacancies;

        public MainPageEmployeeViewModel()
        {
            Vacancies = dataBaseRequests.GetVacancy();
            Vacancy = new ObservableCollection<Vacancy>(Vacancies);
            Categories = new ObservableCollection<Categories>(dataBaseRequests.GetCategories());
            IsVisible = false;
        }

        private ObservableCollection<Vacancy> vacancy;
        public ObservableCollection<Vacancy> Vacancy
        {
            get { return vacancy; }
            set => Set(ref vacancy, value);
        }

        private Professions selectedProfession;
        public Professions SelectedProfession
        {
            get { return selectedProfession; }
            set
            {
                Set(ref selectedProfession, value);
                Vacancy = new ObservableCollection<Vacancy>(Vacancies.Where(v => v.ProfessionId == selectedCategory.Id));
            }
        }

        private Categories selectedCategory;
        public Categories SelectedCategory
        {
            get { return selectedCategory; }
            set
            {
                Set(ref selectedCategory, value);
                Vacancy = new ObservableCollection<Vacancy>(Vacancies.Where(v => dataBaseRequests.GetProfessionById(v.ProfessionId).CategoryId == selectedCategory.Id));
                Professions = new ObservableCollection<Professions>(dataBaseRequests.GetProfessionsByCategory(selectedCategory));
                IsVisible = true;
            }
        }

        private ObservableCollection<Professions> professions;
        public ObservableCollection<Professions> Professions
        {
            get { return professions; }
            set => Set(ref professions, value);
        }

        private ObservableCollection<Categories> categories;
        public ObservableCollection<Categories> Categories
        {
            get { return categories; }
            set => Set(ref categories, value);
        }

        private bool isVisible;
        public bool IsVisible
        {
            get { return isVisible; }
            set => Set(ref isVisible, value);
        }

    }
}
