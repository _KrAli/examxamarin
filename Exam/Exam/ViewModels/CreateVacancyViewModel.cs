﻿using Exam.Interfaces;
using Exam.Models;
using Exam.Services;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.ViewModels
{
    class CreateVacancyViewModel : ViewModelBase
    {
        IDataBaseRequests dataBaseRequests = new DataBaseRequests();

        Accounts accounts;

        public CreateVacancyViewModel()
        {

        }

        private Vacancy vacancy;
        public Vacancy Vacancy
        {
            get { return vacancy; }
            set
            {
                Set(ref vacancy, value);
            }
        }


    }
}
