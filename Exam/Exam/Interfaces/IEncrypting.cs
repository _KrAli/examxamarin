﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Interfaces
{
    public interface IEncrypting
    {

        string CreateSalt();

        string Encrypt(string password, string salt);
    }
}
