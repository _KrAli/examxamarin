﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Interfaces
{
    public interface IValidity
    {
        bool IsValid { get; set; }
    }
}
