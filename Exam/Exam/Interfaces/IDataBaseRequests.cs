﻿using Exam.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Interfaces
{
    public interface IDataBaseRequests
    {
        List<AccountType> GetAccountType();

        int AddAccount(Accounts accounts);

        void AddEmployee(Employee employee);

        void AddEmployer(Employer employer);

        List<Cities> GetCities();

        Accounts GetAccount(string email);

        List<Vacancy> GetVacancy();

        List<Professions> GetProfessions();

        List<Professions> GetProfessionsByCategory(Categories category);

        bool isSameEmail(string email);

        List<Categories> GetCategories();

        Professions GetProfessionById(int? id);
    }
}
