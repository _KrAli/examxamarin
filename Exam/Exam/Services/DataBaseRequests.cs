﻿using Exam.Interfaces;
using Dapper;
using Dapper.Contrib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Exam.Models;
using System.Data.SqlClient;
using Dapper.Contrib.Extensions;

namespace Exam.Services
{
    class DataBaseRequests : IDataBaseRequests
    {

        public string ConnctionString { get; set; }

        public DataBaseRequests()
        {
            ConnctionString = "Data Source=bestjobserver.database.windows.net;Initial Catalog=DataBase;Persist Security Info=True;User ID=GAYadmin;Password=Gayserver1";
        }


        public List<AccountType> GetAccountType()
        {
            using (var connection = new SqlConnection(ConnctionString))
            {
                var list = connection.GetAll<AccountType>().ToList();
                return list;
            }
        }

        public int AddAccount(Accounts accounts)
        {
            using (var connection = new SqlConnection(ConnctionString))
            {
                connection.Insert(accounts);

                var thisAccount = connection.GetAll<Accounts>().Where(x => x.Email == accounts.Email).ToList();

                return thisAccount[0].Id;
            }

        }

        public void AddEmployee(Employee employee)
        {
            using (var connection = new SqlConnection(ConnctionString))
            {
                connection.Insert(employee);

            }

        }

        public void AddEmployer(Employer employer)
        {
            using (var connection = new SqlConnection(ConnctionString))
            {
                connection.Insert(employer);

            }
        }

        public List<Cities> GetCities()
        {
            using (var connection = new SqlConnection(ConnctionString))
            {
                var list = connection.GetAll<Cities>().ToList();
                return list;
            }
        }

        public Accounts GetAccount(string email)
        {
            using (var connection = new SqlConnection(ConnctionString))
            {
                var account = connection.GetAll<Accounts>().Where(x=>x.Email==email).ToList();

                if (account.Count != 0)
                {
                    return account[0];
                }

                return null;
            }
        }

        public List<Vacancy> GetVacancy()
        {
            using (var connection = new SqlConnection(ConnctionString))
            {
                var list = connection.GetAll<Vacancy>().ToList();
                return list;
            }
        }

        public List<Professions> GetProfessions()
        {
            using (var connection = new SqlConnection(ConnctionString))
            {
                var list = connection.GetAll<Professions>().ToList();
                return list;
            }
        }

        public List<Categories> GetCategories()
        {
            using (var connection = new SqlConnection(ConnctionString))
            {
                var list = connection.GetAll<Categories>().ToList();
                return list;
            }
        }

        public Professions GetProfessionById (int? id)
        {
            using (var connection = new SqlConnection(ConnctionString))
            {
                var account = connection.GetAll<Professions>().Where(x => x.Id == id).ToList()[0];
                return account;
            }
        }

        public bool isSameEmail(string email)
        {
            using (var connection = new SqlConnection(ConnctionString))
            {
                var account = connection.GetAll<Accounts>().Where(x => x.Email == email).ToList();
                return account.Count != 0;
            }
        }

        public List<Professions> GetProfessionsByCategory(Categories category)
        {
            using (var connection = new SqlConnection(ConnctionString))
            {
                var list = connection.GetAll<Professions>().Where(p => p.CategoryId == category.Id).ToList();
                return list;
            }
        }
    }
}
