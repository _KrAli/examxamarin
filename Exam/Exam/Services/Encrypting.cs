﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Exam.Interfaces;

namespace Exam.Services
{
    class Encrypting : IEncrypting
    {

        public string CreateSalt()
        {
            byte[] salt = new byte[16];
            new RNGCryptoServiceProvider().GetBytes(salt);

            string savedSalt = Convert.ToBase64String(salt);

            return savedSalt;
        }

        public string Encrypt(string password,string salt)
        {

            var saltByte = Encoding.Unicode.GetBytes(salt);

            var PBKDF2 = new Rfc2898DeriveBytes(password, saltByte, 21);
            var passHash = PBKDF2.GetBytes(36);

            var savedPassword = Convert.ToBase64String(passHash);

            return savedPassword;
        }
    }
}
