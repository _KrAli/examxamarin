﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Services
{
    public static class AccountSettings
    {
        private static ISettings AppSettings => CrossSettings.Current;

        public static string Email
        {
            get => AppSettings.GetValueOrDefault(nameof(Email), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(Email), value);
        }

        public static string Hash
        {
            get => AppSettings.GetValueOrDefault(nameof(Hash), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(Hash), value);
        }
    }
}
