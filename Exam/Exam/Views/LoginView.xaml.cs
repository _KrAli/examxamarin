﻿using Exam.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using GalaSoft.MvvmLight;
using Exam.Enum;

namespace Exam.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginView : ContentPage
	{
		public LoginView ()
		{
			InitializeComponent ();


            BindingContext = new LoginViewModel();

            NavigationPage.SetHasBackButton(this, false);
        }
    }
}