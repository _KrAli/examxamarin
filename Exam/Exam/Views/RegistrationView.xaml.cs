﻿using Exam.Enum;
using Exam.ViewModels;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Exam.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegistrationView : ContentPage
	{
		public RegistrationView ()
		{
			InitializeComponent ();

            BindingContext = new RegistrationViewModel();

            MessagingCenter.Subscribe<ViewModelBase,string>(this, "Test", (sender, param) => {

                DisplayAlert("Mes", param, "Ok");

            });

        }
	}
}