﻿using Exam.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Exam.Views
{

	public partial class EmployeeCreateView : ContentPage
	{
		public EmployeeCreateView()
		{
			InitializeComponent ();

            BindingContext = new EmployeeCreateViewModel();
        }
	}
}