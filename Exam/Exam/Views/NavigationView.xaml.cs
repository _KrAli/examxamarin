﻿using Exam.Enum;
using Exam.ViewModels;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Exam.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NavigationView : ContentPage
	{
		public NavigationView ()
		{
			InitializeComponent ();

            SubscribeMessages();

            BindingContext = new NavigationViewModel();


        }

        private void SubscribeMessages()
        {
            MessagingCenter.Subscribe<ViewModelBase, NavigationName>(this, "Navigation", (sender, param) => {

                if (param == NavigationName.LogIn)
                {
                    Navigation.PushAsync(new LoginView());
                }
                else if (param == NavigationName.Registration)
                {
                    Navigation.PushAsync(new RegistrationView());
                }
                else if (param == NavigationName.EmployeeCreate)
                {
                    Navigation.PushAsync(new EmployeeCreateView());
                }
                else if (param == NavigationName.EmployerCreate)
                {
                    Navigation.PushAsync(new EmployerCreateView());
                }
                else if (param == NavigationName.EmployeeMain)
                {
                    Navigation.PushAsync(new MainPageEmployeeView());
                }

            });
        }

    }
}