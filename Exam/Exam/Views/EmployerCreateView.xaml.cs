﻿using Exam.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Exam.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EmployerCreateView : ContentPage
	{
		public EmployerCreateView ()
		{
			InitializeComponent ();

            BindingContext = new EmployerCreateViewModel();

        }


    }
}