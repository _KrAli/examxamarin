using Exam.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace Exam
{
	public partial class App : Application
	{
		public App ()
		{
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MjAzNzhAMzEzNjJlMzIyZTMwVE9CeGNWK2ZuVnBnVXdDSWJhbUk3UVE2TWg0S1lrdkM3VGxjMVdoMTBvWT0=");

            InitializeComponent();

            MainPage = new NavigationPage(new NavigationView());


        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
