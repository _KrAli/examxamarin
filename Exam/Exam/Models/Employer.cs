﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Models
{
    [Table("Employer")]
    public partial class Employer
    {
        [Key]
        public int Id { get; set; }
        public Nullable<int> AccountId { get; set; }
        public string Contacts { get; set; }
        public string CompanyName { get; set; }
        public string WebSite { get; set; }
        public Nullable<int> CityId { get; set; }
        public string Note { get; set; }
    }
}
