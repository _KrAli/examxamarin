﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Models
{
    public partial class FavoritesEmployee
    {
        public int Id { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> ResumeId { get; set; }

    }
}
