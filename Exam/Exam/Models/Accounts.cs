﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Models
{
    [Table("Accounts")]
    public  class Accounts
    {
        [Key]
        public int Id { get; set; }
        public string Email { get; set; }
        public string Hash { get; set; }
        public string Salt { get; set; }
        public string Fullname { get; set; }
        public int TypeId { get; set; }


    }

}
