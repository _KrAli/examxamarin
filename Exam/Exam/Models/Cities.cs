﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Models
{
    [Table("Cities")]
    public  class Cities
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
