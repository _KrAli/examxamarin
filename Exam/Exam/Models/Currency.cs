﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Models
{
    public  class Currency
    {
        public int Id { get; set; }
        public Nullable<decimal> USD { get; set; }
        public string Name { get; set; }

    }
}
