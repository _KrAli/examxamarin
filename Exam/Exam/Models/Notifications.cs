﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Models
{
    public partial class Notifications
    {
        public int Id { get; set; }
        public Nullable<int> AccountId { get; set; }
        public Nullable<int> MessageId { get; set; }

    }
}
