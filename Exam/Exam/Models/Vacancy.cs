﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Models
{
    [Table("Vacancy")]
    public partial class Vacancy
    {
        [Key]
        public int Id { get; set; }
        public Nullable<int> EmployerId { get; set; }
        public Nullable<int> ProfessionId { get; set; }
        public string Education { get; set; }
        public Nullable<int> AgeMin { get; set; }
        public Nullable<int> AgeMax { get; set; }
        public Nullable<decimal> SalaryMin { get; set; }
        public Nullable<decimal> SalaryMax { get; set; }
        public Nullable<int> CityId { get; set; }
        public string Note { get; set; }
        public string QRcode { get; set; }

    }
}
