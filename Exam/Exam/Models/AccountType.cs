﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Models
{
    [Table("AccountType")]
    public class AccountType
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
