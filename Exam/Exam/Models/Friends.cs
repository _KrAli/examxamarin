﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Models
{
    public partial class Friends
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int FriendId { get; set; }
    }
}
