﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Models
{
    [Table("Employee")]
    public  class Employee
    {
        [Key]
        public int Id { get; set; }
        public Nullable<int> AccountId { get; set; }
        public string Phone { get; set; }
        public string Education { get; set; }
        public Nullable<decimal> Experience { get; set; }
        public Nullable<DateTime> DateOfBirth { get; set; }
        public string PreviousJob { get; set; }
        public string Note { get; set; }
        public string Icon { get; set; }
    }
}
