﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Models
{
    public partial class Resume
    {
        public int Id { get; set; }
        public Nullable<int> EmployeeId { get; set; }
        public Nullable<int> ProfessionId { get; set; }
        public Nullable<decimal> SalaryMin { get; set; }
        public Nullable<decimal> SalaryMax { get; set; }
        public string Note { get; set; }
        public string QRcode { get; set; }
        public string PDFlink { get; set; }
    }
}
