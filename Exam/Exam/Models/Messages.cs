﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Models
{
    public partial class Messages
    {

        public int Id { get; set; }
        public Nullable<int> ChatId { get; set; }
        public Nullable<int> AccountId { get; set; }
        public string Message { get; set; }
        public Nullable<bool> IsRead { get; set; }

    }
}
