﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam.Models
{
    public partial class Settings
    {
        public int Id { get; set; }
        public Nullable<int> AccountId { get; set; }
        public Nullable<int> CurrencyId { get; set; }
        public Nullable<bool> Privacy { get; set; }
    }
}
